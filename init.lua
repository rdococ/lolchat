local store = minetest.get_mod_storage()
local prefix = "--> "

local function say(text, pre)
	if pre then text = prefix .. text end
	minetest.chat_send_all(text)
	if irc then irc:say(text) end
end
local function tell(name, text, pre)
	if pre then text = prefix .. text end
	minetest.chat_send_player(name, text)
end

local votekicks = {}
-- Votekicks are structured as follows: {
--     griefer: {
--         [1]=victim
--         [2]=supporter
--         [3]=lol123
--     }
-- }

minetest.register_chatcommand("yay", {
	description="Yay!",
	privs={shout=true, yay=true},
	func=function(name, param) -- Adapted from a command typed up by cheapie for lols.
		local str="<" .. name .. "> "..(math.random(0,1)==1 and "Y" or "y")
		local max=math.random(1,20)
		for i=1,max,1 do
			str=str..(math.random(0,1)==1 and "A" or "a")
		end
		str=str..(math.random(0,1)==1 and "Y" or "y")
		max = math.random(1, 10)
		for i=1,max,1 do
			str=str..(math.random(0,1)==1 and "!" or "1")
		end
		say(str) 
	end
})

minetest.register_chatcommand("my", {
	params="<object> <action>",
	description="Display chat action using the possessive case (e.g., \"/my cat farts\" displays \"<player name>'s cat farts\")",
	privs={shout=true},
	func=function(name, param)
		say("* " .. name .. "'s " .. param)
	end
})

minetest.register_chatcommand("announce", {
	params="<message>",
	description="Display a message to chat (and IRC if applicable) (e.g. \"Message. ~ user\"). Requires announce privilege.",
	privs={announce=true},
	func=function(name, param)
		say(param .. " ~ " .. name)
	end
})

minetest.register_chatcommand("votekick", {
	params="<username>",
	description="Vote to kick the person. If they get a majority of the players' votes, they are kicked.",
	privs={},
	func=function(name, param)
		local pnum = #minetest.get_connected_players()
		if not param then param = "" end
		if name == param then -- Player wants to kick self. Let them. Hang a lampshade on it, too.
			say(name .. " decided to quit in style by votekicking themselves.", true)
			minetest.kick_player(param, "You wanted to kick yourself, so here you go.")
			--[[if not minetest.get_player_privs(param).votekick then
				if minetest.get_player_privs(param).votekick_bypass then
					minetest.kick_player(param, "What?! Why are you looking at me?! You wanted to be kic-- wait... O_o Why do you have votekick_bypass but not votekick? That's really, really odd. Well, whatever. Bye! :P")
				else
					minetest.kick_player(param, "What?! Why are you looking at me?! You wanted to be kicked! Or did you think your lack of votekick privileges would protect you? :P")
				end
			elseif minetest.get_player_privs(param).votekick_bypass then
				minetest.kick_player(param, "What?! Why are you looking at me?! You wanted to be kicked! Or did you think your votekick_bypass would protect you? :P")
			else
				minetest.kick_player(param, "What?! Why are you looking at me?! You wanted to be kicked! :P")
			end]]
			return
		end
		
		if not minetest.get_player_privs(name).votekick then
			-- We fake the not-enough-perm message here so we can kick the player if they want to votekick themselves anyway.
			tell(name, "You don't have permission to run this command (missing privileges: votekick)", false)
			return
		end
		
		if pnum < 3 then
			tell(name, "You cannot votekick when there " .. (pnum > 1 and "are" or "is") .. " only " .. pnum .. " player" .. (pnum > 1 and "s" or "") .. ".", true)
			return
		end
		
		if not minetest.get_player_by_name(param) then tell(name, param .. " isn't even here!", true); return end
		if minetest.get_player_privs(param).votekick_bypass then tell(name, param .. " has votekick bypass. If they are abusing it, tell an admin.", true); return end
		
		local votekick = votekicks[param]
		if not votekick then votekicks[param] = {}; votekick = votekicks[param] end
		
		for i,kicker in pairs(votekick) do
			if name == kicker then 
				tell(name, "You've already voted to kick " .. param .. ".", true)
				return
			end
		end
		
		table.insert(votekicks[param], name)
		
		say(name .. " votes to kick " .. param .. ".", true)
		
		local requirement = math.floor((pnum / (pnum ^ (1/3))) + 0.25)
		local votes = #votekicks[param]
		if votes >= requirement then
			say(param .. " was kicked by vote.", true)
			minetest.kick_player(param, "Enough people have voted to kick you (" .. votes .. " people, to be exact).")
		end
	end
})
minetest.register_on_leaveplayer(function(player)
	local name = player:get_player_name()
	for kickee,kickers in pairs(votekicks) do
		for i,kicker in pairs(kickers) do
			if kicker == name then table.remove(votekicks[kickee], i) end
		end
		if kickee == name then votekicks[kickee] = nil end
	end
end)

minetest.register_chatcommand("mail", {
	params="<username> <message>",
	description="Sends a message to a player when they join.",
	privs={shout=true},
	func=function(name, param)
		local recipient = string.match(param, "[^%s]+")
		local message = string.match(param, "[^%s]+%s+(.+)")
		if recipient == "" or not recipient then
			tell(name, "Er... I don't think anyone has a blank name.", true)
			return
		elseif message == "" or not message then
			tell(name, "What's the point of sending nothing?!", true)
			return
		elseif name == recipient then
			tell(name, "What's the point of sending a message to yourself?!", true)
			return
		end
		
		if minetest.get_player_by_name(recipient) then
			tell(recipient, "PM from " .. name .. ": " .. message, false)
			tell(name, recipient .. " is here, but I've sent a PM.", true)
			return
		else
			local messages = {}
			if store:get_string("mail") ~= nil and store:get_string("mail") ~= "" then
				messages = minetest.deserialize(store:get_string("mail"))
				if not messages[recipient] then messages[recipient] = {} end
				if #messages[recipient] > 4 then -- avoid spam
					tell(name, "Sorry, but " .. recipient .. " will already receive 5 messages when he logs in. To avoid spam, no more will be sent.", true)
					return
				end
			else
				messages[recipient] = {}
			end
			table.insert(messages[recipient], {name, message})
			store:set_string("mail", minetest.serialize(messages))
			tell(name, "Noted. I will send a message to " .. recipient .. " when they are online.", true)
		end
	end
})

minetest.register_chatcommand("setwelcome", {
	params="<message>",
	description="Set the welcome message. Occurences of \"<player>\" will be replaced with the player's nickname.",
	privs={set_welcome=true},
	func=function(name, param)
		store:set_string("welcome", param or "")
		if (param or ""):len() > 0 then
			tell(name, "Successfully set the welcome message. Here's what it will look like:", true)
			tell(name, param:gsub("<player>", name), true)
		else
			tell(name, "Successfully removed the welcome message.", true)
		end
	end
})

minetest.register_on_joinplayer(function(player)
	local name = player:get_player_name()
	if store:get_string("mail") ~= nil and store:get_string("mail") ~= "" then
		local messages = minetest.deserialize(store:get_string("mail"))
		for _,msg in pairs(messages[name] or {}) do
			tell(name, "Mail from " .. msg[1] .. ": " .. msg[2], false)
		end
		
		messages[name] = {}
		store:set_string("mail", minetest.serialize(messages))
	end
	
	local welcome = store:get_string("welcome")
	if welcome and welcome:len() > 0 then
		tell(name, welcome:gsub("<player>", name), true)
	end
end)

minetest.register_privilege("votekick_bypass", {
	description = "Player cannot be kicked by majority vote.",
	give_to_singleplayer = false
})
minetest.register_privilege("votekick", {
	description = "Player can vote to kick someone.",
	give_to_singleplayer = false
})
minetest.register_privilege("announce", {
	description = "Player can use /announce to announce messages (e.g. \"Message. ~ user\")",
	give_to_singleplayer = false
})
minetest.register_privilege("yay", {
	description = "YAaaaAAaaY!!11!",
	give_to_singleplayer = false
})
minetest.register_privilege("set_welcome", {
	description = "Player can set the welcome message.",
	give_to_singleplayer = false
})

--[[ GOAL: Function that takes a list of vectors, and a vector, and determines which one is closest.
local function match(selections, selector)
	local distance = math.huge
	local isofar, sofar = nil, nil
	for i,v in pairs(selections) do
		if vector.distance(v, selector) < distance then
			distance = vector.distance(v, selector)
			isofar = i
			sofar = v
		end
	end
	return isofar, sofar
end

local hex_digits = {}
for i = 0, 9 do
	hex_digits[tostring(i)] = i
end
hex_digits["a"] = 10
hex_digits["b"] = 11
hex_digits["c"] = 12
hex_digits["d"] = 13
hex_digits["e"] = 14
hex_digits["f"] = 15

local hex_digits_inv = {}
for i,j in pairs(hex_digits) do
	hex_digits_inv[j] = i
end

local function hex_to_vector(hex)
	local d = "[0-9a-f]"
	
	local _, _, r, g, b = hex:find("#(" .. d .. d .. ")(" .. d .. d .. ")(" .. d .. d .. ")")
	
	local R = hex_digits[r:sub(2, 2):lower()] + hex_digits[r:sub(1, 1):lower()] * 16
	local G = hex_digits[g:sub(2, 2):lower()] + hex_digits[g:sub(1, 1):lower()] * 16
	local B = hex_digits[b:sub(2, 2):lower()] + hex_digits[b:sub(1, 1):lower()] * 16
	
	return {x = R, y = G, z = B}
end
local function vector_to_hex(vec)
	local s = "#"
	s = s .. hex_digits_inv[math.floor(vec.x / 16)] .. hex_digits_inv[vec.x % 16]
	s = s .. hex_digits_inv[math.floor(vec.y / 16)] .. hex_digits_inv[vec.y % 16]
	s = s .. hex_digits_inv[math.floor(vec.z / 16)] .. hex_digits_inv[vec.z % 16]
	
	return s
end

assert(vector_to_hex(hex_to_vector("#ff8080")) == "#ff8080")

local underline_char = "̲"

--[=[minetest.register_on_chat_message(function(name, message)
	if not message then return end
	local formatted = message
	
	formatted = formatted:gsub("%[color=(#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f])%]", "(c@%1)")
	formatted = formatted:gsub("%[/color%]", "(c@#ffffff)")
	
	formatted = formatted:gsub("%[u%].+%[/u%]", function (text)
		local new_text = ""
		
		local is_colorcode = false
		local lastchar_colorcode = false
		local length_wo_last_colorcode = 0
		local COLORCODE_START = minetest.get_color_escape_sequence("#ffffff"):sub(1, 1)
		local COLORCODE_END = ")"
		for i = 4, text:len() - 4 do
			local char = text:sub(i, i)
			if char == COLORCODE_START then
				is_colorcode = true
				lastchar_colorcode = true
				new_text = new_text .. char
			elseif char == COLORCODE_END and is_colorcode == true then
				is_colorcode = false
				new_text = new_text .. char
			elseif is_colorcode then
				new_text = new_text .. char
			else
				new_text = new_text .. char .. underline_char
				lastchar_colorcode = false
				length_wo_last_colorcode = #new_text
			end
		end
		
		-- The combining low line is two bytes, and Lua's string manipulation functions seem to be primitive.
		return lastchar_colorcode and  -- If the last character was part of a color code, we obviously don't want to remove that.
			new_text:sub(1, length_wo_last_colorcode - 2) ..  -- So, we remove the last underlining character that /wasn't/ a color code.
				new_text:sub(length_wo_last_colorcode + 1, new_text:len()) or  -- Don't forget to readd the color code afterwards.
			new_text:sub(1, new_text:len() - 2)
	end)
	
	minetest.chat_send_all("<" .. name .. "> " .. formatted)  -- IRC is handled below.
	return true
end)]=]

if irc then -- Extra feature! Enable italics and stuff to show up like /this./ Also very messy - HORRAY!
	local irc_to_color = {}
	local vector_palette = {}
	local color_to_irc = {}

	irc_to_color["1"] = "#ffffff"
	irc_to_color["2"] = "#0000ff"
	irc_to_color["3"] = "#00ff00"
	irc_to_color["4"] = "#ff0000"
	irc_to_color["5"] = "#a06000"
	irc_to_color["6"] = "#8000ff"
	irc_to_color["7"] = "#ff8000"
	irc_to_color["8"] = "#ffff00"
	irc_to_color["9"] = "#80ff00"
	irc_to_color["10"] = "#00ffff"
	irc_to_color["11"] = "#00a0ff"
	irc_to_color["12"] = "#0080ff"
	irc_to_color["13"] = "#ff00ff"
	irc_to_color["14"] = "#b0b0b0"
	irc_to_color["15"] = "#808080"
	irc_to_color["16"] = "#606060"
	
	for i,j in pairs(irc_to_color) do
		color_to_irc[j] = i
		vector_palette[tonumber(i)] = hex_to_vector(j)
	end
	
	color_to_irc["#ffffff"] = "0"
	color_to_irc["#000000"] = "1"
	color_to_irc["#b0b0b0"] = "16"
	color_to_irc["#606060"] = "14"
	
	local function normalize(text)
		-- Strip colors.
		-- text = text:gsub("\3%d[%d,]*", "")
		
		-- Here, we find IRC color sequences of the form "K12,34" and feed the captures through a second gsub which replaces ",34" with "".
		text = text:gsub("%d%d?,%d%d?", function (txt)  -- K12,34
			txt = string.gsub(txt, ",%d%d?", "")  -- strip ,34
			return txt  -- K12
		end)
		
		-- Here is the magic: we take sequences of the form "K12", remove the K, and then match them to a palette above.
		text = text:gsub("%d%d?", function (txt)
			txt = string.gsub(txt, "", "")  -- strip K
			return minetest.get_color_escape_sequence(irc_to_color[txt] or "#ffffff")  -- match the number to the palette above.
		end)
		
		text = text:gsub("", minetest.get_color_escape_sequence("#ffffff"))
		
		-- Replace italics with /this./
		text = text:gsub("", "/")
		
		-- Replace bold with *this.*
		text = text:gsub("\2", "*")
		
		-- Replace underline with _this._
		-- TODO: speed up this code by using string manipulation functions.
		local old_text = text
		text = ""
		local is_underlined = false
		local is_colorcode = false
		local COLORCODE_START = minetest.get_color_escape_sequence("#ffffff"):sub(1, 1)
		local COLORCODE_END = ")"
		for i = 1, old_text:len() do
			local char = old_text:sub(i, i)
			if char == "\31" then
				is_underlined = not is_underlined
				if not is_underlined then text = text:sub(1, text:len() - 2) end
			elseif char == COLORCODE_START then
				is_colorcode = true
				text = text .. char
			elseif is_colorcode and char == COLORCODE_END then
				is_colorcode = false
				text = text .. char
			else
				if is_underlined and not is_colorcode then text = text .. char .. underline_char else text = text .. char end
			end
		end

		return is_underlined and text:sub(1, text:len() - 2) or text
	end
	
	function irc.hooks.channelChat(msg)
		local text = normalize(msg.args[2])

		irc.check_botcmd(msg)

		-- Don't let a user impersonate someone else by using the nick "IRC"
		local fake = msg.user.nick:lower():match("^[il|]rc$")
		if fake then
			irc.sendLocal("<"..msg.user.nick.."@IRC> "..text)
			return
		end

		-- Support multiple servers in a channel better by converting:
		-- "<server@IRC> <player> message" into "<player@server> message"
		-- "<server@IRC> *** player joined/left the game" into "*** player joined/left server"
		-- and "<server@IRC> * player orders a pizza" into "* player@server orders a pizza"
		local foundchat, _, chatnick, chatmessage =
			text:find("^<([^>]+)> (.*)$")
		local foundjoin, _, joinnick =
			text:find("^%*%*%* ([^%s]+) joined the game$")
		local foundleave, _, leavenick =
			text:find("^%*%*%* ([^%s]+) left the game$")
		local foundaction, _, actionnick, actionmessage =
			text:find("^%* ([^%s]+) (.*)$")
		-- Add support for "<server@IRC> *** player left the game (Timed out)"
		local foundping, _, pingnick =
			text:find("^%*%*%* ([^%s]+) left the game %(Timed out%)$")

		if text:sub(1, 5) == "[off]" then
			return
		elseif foundchat then
			irc.sendLocal(("<%s@%s> %s")
					:format(chatnick, msg.user.nick, chatmessage))
		elseif foundjoin then
			irc.sendLocal(("*** %s joined %s")
					:format(joinnick, msg.user.nick))
		elseif foundleave then
			irc.sendLocal(("*** %s left %s")
					:format(leavenick, msg.user.nick))
		elseif foundping then
			irc.sendLocal(("*** %s left %s (Timed out)")
					:format(pingnick, msg.user.nick))
		elseif foundaction then
			irc.sendLocal(("* %s@%s %s")
					:format(actionnick, msg.user.nick, actionmessage))
		else
			irc.sendLocal(("<%s@IRC> %s"):format(msg.user.nick, text))
		end
	end
	
	irc.registered_hooks["OnChannelChat"] = {irc.hooks.channelChat}
	
	--[=[function irc.playerMessage(name, message)
		message = message:gsub("%[/?b%]", "\2")
		message = message:gsub("%[/?i%]", "")
		message = message:gsub("%[/?u%]", "\31")
		
		-- TODO: Figure out the nearest IRC color to the HEX color, and use that.
		message = message:gsub("%[color=(#[0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f][0-9a-f])%]", function (capture)
			local color = tostring(match(vector_palette, hex_to_vector(capture)))
			if color then
				return "" .. color
			else
				return ""
			end
		end)
		message = message:gsub("%[/color%]", "")
		
		if type(name) == "table" then
			error("If you see this message, my code has detected that name is a table. I guess you'd have to go, \"Hey {}!\".")
		elseif type(message) == "table" then
			error("If you see this message, my code has detected that message is a table despite the fact that the mod was just performing string manipulation functions on it.")
		end
		
		return ("<%s> %s"):format(name, message)
	end]=]
end]]
