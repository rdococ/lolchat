# Lolchat
This is the Lolchat mod for Minetest. It adds various fun commands, and some useful ones.

/yay
----
YAaaAAaaY!!11!

Privileges required: yay, shout

/my
---
Similar to /me, but uses the possessive case. For example:
* /my chicken farts
* * player123's chicken farts

Privileges required: shout

/announce
---------
Announces something to the chat, placing your name at the end rather than the beginning.
* /announce Yay!
* Yay! ~ player123

Privileges required: announce

/votekick
---------
Allows you to vote to kick someone. If enough people votekick someone, they are forced to leave.
* /votekick evilTroll666
* * player123 votes to kick evilTroll666.
* * evilTroll666 was kicked by majority vote.

Privileges required: votekick
